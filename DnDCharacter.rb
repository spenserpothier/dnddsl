class DnDCharacter
  attr_accessor :name, :personality_traits, :ideals

  def initialize(name = '', &block)
    @name = name.to_s
    @personality_traits = []
    @ideals = []
    @attributes = []
    instance_eval &block
    # @out = ''
  end

  def personality_trait(description)
    @personality_traits << description
  end

  def ideal(description)
    @ideals << description
  end

  def attribute(name, value)
    att = {}
    att[:name] = name
    att[:value] = value
    @attributes << att
  end

  def to_s
    output = "Character Name: #{@name}\n"

    @personality_traits.each_with_index do |t, index|
      output << "Trait #{index + 1}: #{t}\n"
    end

    @ideals.each_with_index do |i, index|
      output << "Ideal #{index + 1}: #{i}\n"
    end

    @attributes.each do |a|
      output << "Attribute - #{a[:name].capitalize}: #{a[:value]}\n"
    end
    output
  end
end
