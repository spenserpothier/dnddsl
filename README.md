# DnD DSL

## Description
DnD DSL is a Domain Specific Langauge for DnD. Why did I create this you ask? Well, for one, I wanted to take to separate nerdy activities and make them both nerdier. But in actuality I have been learning Ruby and wanted to learn more about meta-programming, and some friends are going to be teaching me how to play DnD. From what I know about DnD already, I felt like it would be a good candidate to make a DSL for.

## To-Do
  - [] Complete README with more information
  - [] Add different things to create (Monsters, Bosses, Campaigns, etc.)
  - [] Character sheet todos
    - [] Bonds
    - [] Flaws
    - [] Hit Points (max, current, temp)
    - [] Armor Class
    - [] Initiative
    - [] Speed

    - [] Skills
    - [] Saving Throws

    - [] Equipment
    - [] Attacks & Spells
    - [] Features & Traits
    - [] Other proficiencies & languages

    - [] Class & Level
    - [] Background
    - [] Faction
    - [] Races
    - [] Alignment
    - [] Experience Points
    - [] DCI Number
    - [] Age
    - [] Height
    - [] Weight
    - [] Eyes
    - [] Skin
    - [] Hair

    - [] Character Backstory
    - [] Additional Features & Traits
    - [] Treasure
    - [] Allies & Organizations

